package com.example.config;

import com.example.model.Course;
import com.example.repository.AttendanceRepository;
import com.example.repository.CourseRepository;
import com.example.repository.EnrollmentRepository;
import com.example.repository.GradeRepository;

public class RepositoryConfig {
    public static CourseRepository getCourseRepository() {
        CourseRepository courseRepository = new CourseRepository();
        courseRepository.add(new Course("idCourse0", "Some Course 0", 10));
        courseRepository.add(new Course("idCourse1", "Some Course 1", 5));
        return courseRepository;
    }

    public static EnrollmentRepository getEnrollmentRepository(CourseRepository courseRepository) {
        return new EnrollmentRepository(courseRepository);
    }

    public static AttendanceRepository getAttendanceRepository(CourseRepository courseRepository) {
        return new AttendanceRepository(courseRepository);
    }

    public static GradeRepository getGradeRepository() {
        return new GradeRepository();
    }
}
