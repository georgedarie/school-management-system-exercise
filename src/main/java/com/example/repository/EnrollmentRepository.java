package com.example.repository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class EnrollmentRepository {
    private final CourseRepository courseRepository;
    private final Map<String, HashSet<String>> enrollments;

    public EnrollmentRepository(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
        this.enrollments = new HashMap<>();
    }

    public void add(String courseId, String studentId) throws Exception {
        if (courseRepository.exists(courseId)) {
            HashSet<String> enrolledStudents = enrollments.get(courseId);
            if (enrolledStudents == null) {
                enrolledStudents = new HashSet<>();
            }
            enrolledStudents.add(studentId);
            enrollments.put(courseId, enrolledStudents);
        } else {
            throw new Exception("There is no course with id [" + courseId + "]!");
        }
    }

    public boolean isStudentEnrolled(String courseId, String studentId) {
        HashSet<String> enrolledStudentIds = enrollments.get(courseId);
        return enrolledStudentIds.stream().anyMatch(studentId::equals);
    }
}
