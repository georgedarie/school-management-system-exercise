package com.example.repository;

import com.example.model.Course;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CourseRepository {
    private final Map<String, Course> courses;

    public CourseRepository() {
        courses = new HashMap<>();
    }

    public void add(Course course) {
        courses.put(course.getId(), course);
    }

    public void update(Course course) {
        courses.put(course.getId(), course);
    }

    public Course get(String id) {
        return courses.get(id);
    }

    public List<Course> getAll() {
        return new ArrayList<>(courses.values());
    }

    public boolean exists(String id) {
        return courses.get(id) != null;
    }
}
