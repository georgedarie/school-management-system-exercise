package com.example.repository;

import com.example.model.EnrollmentKey;

import java.util.HashMap;
import java.util.Map;

public class GradeRepository {
    private final Map<EnrollmentKey, Double> grades;

    public GradeRepository() {
        grades = new HashMap<>();
    }

    public void addGrade(EnrollmentKey enrollmentKey, Double value) {
        grades.put(enrollmentKey, value);
    }

    public Double getGrade(EnrollmentKey enrollmentKey) {
        return grades.get(enrollmentKey);
    }
}
