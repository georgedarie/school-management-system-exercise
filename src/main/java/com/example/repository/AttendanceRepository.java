package com.example.repository;

import com.example.model.EnrollmentKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AttendanceRepository {
    private final CourseRepository courseRepository;
    private final Map<EnrollmentKey, ArrayList<Boolean>> attendances;

    public AttendanceRepository(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
        this.attendances = new HashMap<>();
    }

    public void add(EnrollmentKey enrollmentKey) throws Exception {
        String courseId = enrollmentKey.getCourseId();
        if (this.courseRepository.exists(courseId)) {
            ArrayList<Boolean> studentAttendances = this.attendances.get(enrollmentKey);
            if (studentAttendances == null) {
                int numberOfPossibleAttendances = this.courseRepository.get(courseId).getNumberOfAttendances();
                studentAttendances = Stream
                        .generate(() -> Boolean.FALSE)
                        .limit(numberOfPossibleAttendances)
                        .collect(Collectors.toCollection(ArrayList::new));
                this.attendances.put(enrollmentKey, studentAttendances);
            }
            int indexOfAttendance = studentAttendances.indexOf(Boolean.FALSE);
            studentAttendances.set(indexOfAttendance, Boolean.TRUE);
            this.attendances.put(enrollmentKey, studentAttendances);
        } else {
            throw new Exception("There is no course with id [" + courseId + "]!");
        }
    }
}
