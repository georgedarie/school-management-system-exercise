package com.example.model;

import java.util.Objects;

public class EnrollmentKey {
    private final String studentId;
    private final String courseId;

    public EnrollmentKey(String studentId, String courseId) {
        this.studentId = studentId;
        this.courseId = courseId;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getCourseId() {
        return courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnrollmentKey enrollmentKey = (EnrollmentKey) o;
        return studentId.equals(enrollmentKey.studentId) &&
                courseId.equals(enrollmentKey.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, courseId);
    }
}
