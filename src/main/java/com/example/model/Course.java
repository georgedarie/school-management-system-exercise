package com.example.model;

public class Course {
    private final String id;
    private final String title;
    private final Integer numberOfAttendances;
    private String curricula;

    public Course(String id, String title, Integer numberOfAttendances) {
        this.id = id;
        this.title = title;
        this.numberOfAttendances = numberOfAttendances;
    }

    public Course(String id, String title, Integer numberOfAttendances, String curricula) {
        this(id, title, numberOfAttendances);
        this.curricula = curricula;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getNumberOfAttendances() {
        return numberOfAttendances;
    }

    public String getCurricula() {
        return curricula;
    }

    public void setCurricula(String curricula) {
        this.curricula = curricula;
    }
}
