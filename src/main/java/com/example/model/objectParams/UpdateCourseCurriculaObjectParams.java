package com.example.model.objectParams;

public class UpdateCourseCurriculaObjectParams {
    private final String teacherId;
    private final String courseId;
    private final String newCurricula;

    public UpdateCourseCurriculaObjectParams(String teacherId, String courseId, String newCurricula) {
        this.teacherId = teacherId;
        this.courseId = courseId;
        this.newCurricula = newCurricula;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public String getCourseId() {
        return courseId;
    }

    public String getNewCurricula() {
        return newCurricula;
    }
}
