package com.example.model.objectParams;

public class GradeStudentObjectParams {
    private final String teacherId;
    private final String studentId;
    private final String courseId;
    private final Double gradeValue;

    public GradeStudentObjectParams(String teacherId, String studentId, String courseId, Double gradeValue) {
        this.teacherId = teacherId;
        this.studentId = studentId;
        this.courseId = courseId;
        this.gradeValue = gradeValue;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getCourseId() {
        return courseId;
    }

    public Double getGradeValue() {
        return gradeValue;
    }
}
