package com.example.service;

import com.example.model.EnrollmentKey;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.model.objectParams.GradeStudentObjectParams;
import com.example.model.objectParams.UpdateCourseCurriculaObjectParams;
import com.example.repository.AttendanceRepository;
import com.example.repository.CourseRepository;
import com.example.repository.EnrollmentRepository;
import com.example.repository.GradeRepository;

import java.util.HashMap;
import java.util.Map;

public class SessionManager {
    private final AttendanceRepository attendanceRepository;
    private final CourseRepository courseRepository;
    private final EnrollmentRepository enrollmentRepository;
    private final GradeRepository gradeRepository;
    private final Map<String, TeacherActions> teacherConnections;
    private final Map<String, StudentActions> studentConnections;

    public SessionManager(
            AttendanceRepository attendanceRepository,
            CourseRepository courseRepository,
            EnrollmentRepository enrollmentRepository,
            GradeRepository gradeRepository) {
        this.attendanceRepository = attendanceRepository;
        this.courseRepository = courseRepository;
        this.enrollmentRepository = enrollmentRepository;
        this.gradeRepository = gradeRepository;
        this.teacherConnections = new HashMap<>();
        this.studentConnections = new HashMap<>();
    }

    // Teacher actions

    public void connectTeacher(Teacher teacher) {
        String teacherId = teacher.getId();
        TeacherActions teacherActions = new TeacherActions(
                this.gradeRepository,
                this.enrollmentRepository,
                this.courseRepository
        );
        this.teacherConnections.put(teacherId, teacherActions);
    }

    public void disconnectTeacher(String teacherId) {
        this.teacherConnections.remove(teacherId);
    }

    public void startTeachingCourse(String teacherId, String courseId) throws Exception {
        TeacherActions teacherActions = this.teacherConnections.get(teacherId);
        if (teacherActions != null) {
            teacherActions.startTeachingCourse(courseId);
        } else {
            throw new Exception("Teacher with id [" + teacherId + "] is not connected!");
        }
    }

    public void stopTeachingCourse(String teacherId) throws Exception {
        TeacherActions teacherActions = this.teacherConnections.get(teacherId);
        if (teacherActions != null) {
            teacherActions.stopTeachingCourse();
        } else {
            throw new Exception("Teacher with id [" + teacherId + "] is not connected!");
        }
    }

    public void gradeStudent(GradeStudentObjectParams objectParams) throws Exception {
        TeacherActions teacherActions = this.teacherConnections.get(objectParams.getTeacherId());
        if (teacherActions != null) {
            EnrollmentKey enrollmentKey = new EnrollmentKey(objectParams.getStudentId(), objectParams.getCourseId());
            teacherActions.gradeStudent(enrollmentKey, objectParams.getGradeValue());
        } else {
            throw new Exception("Teacher with id [" + objectParams.getTeacherId() + "] is not connected!");
        }
    }

    public void updateCourseCurricula(UpdateCourseCurriculaObjectParams objectParams) throws Exception {
        TeacherActions teacherActions = this.teacherConnections.get(objectParams.getTeacherId());
        if (teacherActions != null) {
            teacherActions.updateCourseCurricula(
                    objectParams.getCourseId(),
                    objectParams.getNewCurricula()
            );
        } else {
            throw new Exception("Teacher with id [" + objectParams.getTeacherId() + "] is not connected!");
        }
    }

    private boolean isCourseCurrentlyBeingTaught(String courseId) {
        return this.teacherConnections
                .values()
                .stream()
                .filter(teacherActions -> teacherActions.getCourseCurrentlyTeaching() != null)
                .anyMatch(teacherActions -> teacherActions.getCourseCurrentlyTeaching().getId().equals(courseId));
    }

    // Student actions

    public void connectStudent(Student student) {
        String studentId = student.getId();
        StudentActions studentActions = new StudentActions(
                student,
                this.enrollmentRepository,
                this.gradeRepository,
                this.courseRepository,
                this.attendanceRepository);
        this.studentConnections.put(studentId, studentActions);
    }

    public void disconnectStudent(String studentId) {
        this.studentConnections.remove(studentId);
    }

    public void enrollToCourse(String studentId, String courseId) throws Exception {
        StudentActions studentActions = this.studentConnections.get(studentId);
        if (studentActions != null) {
            studentActions.enrollToCourse(courseId);
        } else {
            throw new Exception("Student with id [" + studentId + "] is not connected!");
        }
    }

    public void attendCourse(EnrollmentKey enrollmentKey) throws Exception {
        String studentId = enrollmentKey.getStudentId();
        String courseId = enrollmentKey.getCourseId();
        StudentActions studentActions = this.studentConnections.get(studentId);
        if (studentActions != null) {
            if (isCourseCurrentlyBeingTaught(courseId)) {
                studentActions.attendCourse(courseId);
            } else {
                throw new Exception("The course with id [" + courseId + "] is not being taught at the moment!");
            }
        } else {
            throw new Exception("Student with id [" + studentId + "] is not connected!");
        }
    }

    public Double getGrade(String studentId, String courseId) throws Exception {
        StudentActions studentActions = this.studentConnections.get(studentId);
        if (studentActions != null) {
            return studentActions.getGrade(courseId);
        } else {
            throw new Exception("Student with id [" + studentId + "] is not connected!");
        }
    }
}
