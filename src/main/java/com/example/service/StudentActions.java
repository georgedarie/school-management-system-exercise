package com.example.service;

import com.example.model.EnrollmentKey;
import com.example.model.Student;
import com.example.repository.AttendanceRepository;
import com.example.repository.CourseRepository;
import com.example.repository.EnrollmentRepository;
import com.example.repository.GradeRepository;

public class StudentActions {
    private final Student connectedStudent;
    private final EnrollmentRepository enrollmentRepository;
    private final GradeRepository gradeRepository;
    private final CourseRepository courseRepository;
    private final AttendanceRepository attendanceRepository;

    public StudentActions(
            Student connectedStudent,
            EnrollmentRepository enrollmentRepository,
            GradeRepository gradeRepository,
            CourseRepository courseRepository,
            AttendanceRepository attendanceRepository) {
        this.connectedStudent = connectedStudent;
        this.enrollmentRepository = enrollmentRepository;
        this.gradeRepository = gradeRepository;
        this.courseRepository = courseRepository;
        this.attendanceRepository = attendanceRepository;
    }

    public void enrollToCourse(String courseId) throws Exception {
        if (this.courseRepository.exists(courseId)) {
            this.enrollmentRepository.add(courseId, this.connectedStudent.getId());
        } else {
            throw new Exception("No course with id [" + courseId + "] exists!");
        }
    }

    public void attendCourse(String courseId) throws Exception {
        if (!this.courseRepository.exists(courseId)) {
            throw new Exception("No course with id [" + courseId + "] exists!");
        }
        String connectedStudentId = this.connectedStudent.getId();
        if (this.enrollmentRepository.isStudentEnrolled(courseId, connectedStudentId)) {
            EnrollmentKey enrollmentKey = new EnrollmentKey(connectedStudentId, courseId);
            this.attendanceRepository.add(enrollmentKey);
        } else {
            throw new Exception(
                    "Student with id [" + connectedStudentId + "] " +
                            "is not enrolled in the course with id [" + courseId + "]!"
            );
        }
    }

    public Double getGrade(String courseId) throws Exception {
        if (!this.courseRepository.exists(courseId)) {
            throw new Exception("No course with id [" + courseId + "] exists!");
        }
        if (this.enrollmentRepository.isStudentEnrolled(courseId, this.connectedStudent.getId())) {
            EnrollmentKey enrollmentKey = new EnrollmentKey(this.connectedStudent.getId(), courseId);
            return this.gradeRepository.getGrade(enrollmentKey);
        } else {
            throw new Exception("Student not enrolled to the course with the id [" + courseId + "]!");
        }
    }
}
