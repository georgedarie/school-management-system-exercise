package com.example.service;

import com.example.model.Course;
import com.example.model.EnrollmentKey;
import com.example.repository.CourseRepository;
import com.example.repository.EnrollmentRepository;
import com.example.repository.GradeRepository;

public class TeacherActions {
    private final GradeRepository gradeRepository;
    private final EnrollmentRepository enrollmentRepository;
    private final CourseRepository courseRepository;
    private Course courseCurrentlyTeaching;

    public TeacherActions(
            GradeRepository gradeRepository,
            EnrollmentRepository enrollmentRepository,
            CourseRepository courseRepository) {
        this.gradeRepository = gradeRepository;
        this.enrollmentRepository = enrollmentRepository;
        this.courseRepository = courseRepository;
    }

    public Course getCourseCurrentlyTeaching() {
        return this.courseCurrentlyTeaching;
    }

    public void startTeachingCourse(String courseId) throws Exception {
        if (this.courseRepository.exists(courseId)) {
            this.courseCurrentlyTeaching = this.courseRepository.get(courseId);
        } else {
            throw new Exception("The course with id [" + courseId + "] does not exist!");
        }
    }

    public void stopTeachingCourse() {
        this.courseCurrentlyTeaching = null;
    }

    public void gradeStudent(EnrollmentKey enrollmentKey, Double value) throws Exception {
        String courseId = enrollmentKey.getCourseId();
        String studentId = enrollmentKey.getStudentId();
        if (this.enrollmentRepository.isStudentEnrolled(courseId, studentId)) {
            this.gradeRepository.addGrade(enrollmentKey, value);
        } else {
            throw new Exception(
                    "Student with id [" + studentId + "] not enrolled in course with id [" + courseId + "]!"
            );
        }
    }

    public void updateCourseCurricula(String courseId, String curricula) throws Exception {
        if (this.courseRepository.exists(courseId)) {
            Course courseToUpdate = this.courseRepository.get(courseId);
            courseToUpdate.setCurricula(curricula);
            this.courseRepository.update(courseToUpdate);
        } else {
            throw new Exception("The course with id [" + courseId + "] does not exist!");
        }
    }
}
