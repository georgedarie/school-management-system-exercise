package com.example;

import com.example.config.RepositoryConfig;
import com.example.model.EnrollmentKey;
import com.example.model.Student;
import com.example.model.Teacher;
import com.example.model.objectParams.GradeStudentObjectParams;
import com.example.model.objectParams.UpdateCourseCurriculaObjectParams;
import com.example.repository.AttendanceRepository;
import com.example.repository.CourseRepository;
import com.example.repository.EnrollmentRepository;
import com.example.repository.GradeRepository;
import com.example.service.SessionManager;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SchoolManagementSystemTest {
    private CourseRepository courseRepository;
    private EnrollmentRepository enrollmentRepository;
    private AttendanceRepository attendanceRepository;
    private GradeRepository gradeRepository;
    private ArrayList<Student> students;
    private ArrayList<Teacher> teachers;

    public void setUp() {
        this.courseRepository = RepositoryConfig.getCourseRepository();
        this.enrollmentRepository = RepositoryConfig.getEnrollmentRepository(this.courseRepository);
        this.attendanceRepository = RepositoryConfig.getAttendanceRepository(this.courseRepository);
        this.gradeRepository = RepositoryConfig.getGradeRepository();
        this.students = new ArrayList<>(Arrays.asList(
                new Student("idStudent0", "Student Name One"),
                new Student("idStudent1", "Student Name Two"),
                new Student("idStudent2", "Student Name Three"),
                new Student("idStudent3", "Student Name Four"),
                new Student("idStudent4", "Student Name Five")
        ));
        this.teachers = new ArrayList<>(Arrays.asList(
                new Teacher("idTeacher0", "Teacher Name One"),
                new Teacher("idTeacher1", "Teacher Name Two")
        ));
    }


    @Test
    public void happyFlow() {
        setUp();
        SessionManager sessionManager = new SessionManager(
                this.attendanceRepository,
                this.courseRepository,
                this.enrollmentRepository,
                this.gradeRepository
        );

        // connecting students
        sessionManager.connectStudent(this.students.get(0));
        sessionManager.connectStudent(this.students.get(1));
        sessionManager.connectStudent(this.students.get(2));
        sessionManager.connectStudent(this.students.get(3));
        sessionManager.connectStudent(this.students.get(4));

        // connecting teachers
        sessionManager.connectTeacher(this.teachers.get(0));
        sessionManager.connectTeacher(this.teachers.get(1));

        // enrolling students to courses
        try {
            sessionManager.enrollToCourse(this.students.get(0).getId(), "idCourse0");
            sessionManager.enrollToCourse(this.students.get(1).getId(), "idCourse0");
            sessionManager.enrollToCourse(this.students.get(2).getId(), "idCourse1");
            sessionManager.enrollToCourse(this.students.get(3).getId(), "idCourse1");
            sessionManager.enrollToCourse(this.students.get(4).getId(), "idCourse1");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        assertTrue(this.enrollmentRepository.isStudentEnrolled("idCourse0", this.students.get(0).getId()));
        assertTrue(this.enrollmentRepository.isStudentEnrolled("idCourse0", this.students.get(1).getId()));
        assertTrue(this.enrollmentRepository.isStudentEnrolled("idCourse1", this.students.get(2).getId()));
        assertTrue(this.enrollmentRepository.isStudentEnrolled("idCourse1", this.students.get(3).getId()));
        assertTrue(this.enrollmentRepository.isStudentEnrolled("idCourse1", this.students.get(4).getId()));

        // define course curricula
        UpdateCourseCurriculaObjectParams objectParams0 = new UpdateCourseCurriculaObjectParams(
                this.teachers.get(0).getId(),
                "idCourse0",
                "This is course 0. It will be about something."
        );
        UpdateCourseCurriculaObjectParams objectParams1 = new UpdateCourseCurriculaObjectParams(
                this.teachers.get(1).getId(),
                "idCourse1",
                "This is course 1. It will be about something else."
        );
        try {
            sessionManager.updateCourseCurricula(objectParams0);
            sessionManager.updateCourseCurricula(objectParams1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String course0Curricula = this.courseRepository.get("idCourse0").getCurricula();
        String course1Curricula = this.courseRepository.get("idCourse1").getCurricula();
        assertEquals("This is course 0. It will be about something.", course0Curricula);
        assertEquals("This is course 1. It will be about something else.", course1Curricula);

        // start teaching
        try {
            sessionManager.startTeachingCourse(this.teachers.get(0).getId(), "idCourse0");
            sessionManager.startTeachingCourse(this.teachers.get(1).getId(), "idCourse1");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // attend classes
        try {
            sessionManager.attendCourse(new EnrollmentKey(this.students.get(0).getId(), "idCourse0"));
            sessionManager.attendCourse(new EnrollmentKey(this.students.get(1).getId(), "idCourse0"));
            sessionManager.attendCourse(new EnrollmentKey(this.students.get(2).getId(), "idCourse1"));
            sessionManager.attendCourse(new EnrollmentKey(this.students.get(3).getId(), "idCourse1"));
            sessionManager.attendCourse(new EnrollmentKey(this.students.get(4).getId(), "idCourse1"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // grade students
        try {
            sessionManager.gradeStudent(new GradeStudentObjectParams(
                    this.teachers.get(0).getId(),
                    this.students.get(0).getId(),
                    "idCourse0",
                    5.0
            ));
            sessionManager.gradeStudent(new GradeStudentObjectParams(
                    this.teachers.get(0).getId(),
                    this.students.get(1).getId(),
                    "idCourse0",
                    6.0
            ));
            sessionManager.gradeStudent(new GradeStudentObjectParams(
                    this.teachers.get(1).getId(),
                    this.students.get(2).getId(),
                    "idCourse1",
                    7.0
            ));
            sessionManager.gradeStudent(new GradeStudentObjectParams(
                    this.teachers.get(1).getId(),
                    this.students.get(3).getId(),
                    "idCourse1",
                    8.0
            ));
            sessionManager.gradeStudent(new GradeStudentObjectParams(
                    this.teachers.get(1).getId(),
                    this.students.get(4).getId(),
                    "idCourse1",
                    9.0
            ));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // checking grades
        Double student0GradeInCourse0 = 0.0;
        Double student1GradeInCourse0 = 0.0;
        Double student2GradeInCourse1 = 0.0;
        Double student3GradeInCourse1 = 0.0;
        Double student4GradeInCourse1 = 0.0;
        try {
            student0GradeInCourse0 = sessionManager.getGrade(this.students.get(0).getId(), "idCourse0");
            student1GradeInCourse0 = sessionManager.getGrade(this.students.get(1).getId(), "idCourse0");
            student2GradeInCourse1 = sessionManager.getGrade(this.students.get(2).getId(), "idCourse1");
            student3GradeInCourse1 = sessionManager.getGrade(this.students.get(3).getId(), "idCourse1");
            student4GradeInCourse1 = sessionManager.getGrade(this.students.get(4).getId(), "idCourse1");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        assertEquals(5.0, student0GradeInCourse0, 0.01);
        assertEquals(6.0, student1GradeInCourse0, 0.01);
        assertEquals(7.0, student2GradeInCourse1, 0.01);
        assertEquals(8.0, student3GradeInCourse1, 0.01);
        assertEquals(9.0, student4GradeInCourse1, 0.01);

        // stop teaching courses
        try {
            sessionManager.stopTeachingCourse(this.teachers.get(0).getId());
            sessionManager.stopTeachingCourse(this.teachers.get(1).getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // disconnect students
        sessionManager.disconnectStudent(this.students.get(0).getId());
        sessionManager.disconnectStudent(this.students.get(1).getId());
        sessionManager.disconnectStudent(this.students.get(2).getId());
        sessionManager.disconnectStudent(this.students.get(3).getId());
        sessionManager.disconnectStudent(this.students.get(4).getId());

        // disconnect teachers
        sessionManager.disconnectTeacher(this.teachers.get(0).getId());
        sessionManager.disconnectTeacher(this.teachers.get(1).getId());

    }
}
