**Students can:**

    * Enroll to courses;
    * Attend courses;
    * Check grades.
    
**Teachers can:**

    * Teach courses;
    * Grade courses;
    * Define course curricula.